//
//  Movement.swift
//  my game
//
//  Created by Romain Margheriti on 18/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit

protocol Movable {
    func computeNewPosition(dt: TimeInterval) -> CGPoint
    func approach(flGoal: CGFloat, flCurrent: CGFloat, dt: TimeInterval) ->CGFloat
}
