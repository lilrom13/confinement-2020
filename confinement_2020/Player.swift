//
//  File.swift
//  my game
//
//  Created by Romain Margheriti on 17/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit

class Player: MovableEntity {
    private var level = 0
    
    private var node = SKSpriteNode()
    
    private var ship = SKSpriteNode(imageNamed: "Ship_01")
    private var exhaust = SKSpriteNode(imageNamed: "Exhaust_01")
    private var fireShots = SKSpriteNode(imageNamed: "FireShots_01")
    
    override init(position: CGPoint) {
        super.init(position: position)
        
        let scale: CGFloat = 1
        self.setScale(scale)
        
        let shipRec = SKShapeNode(rectOf: CGSize(width: ship.size.width, height: ship.size.height))
        let exhaustRec = SKShapeNode(rectOf: CGSize(width: exhaust.size.width, height: exhaust.size.height))
        let center = SKShapeNode(circleOfRadius: CGFloat(1))

        exhaust.position = CGPoint(x: 0, y: -1 * (ship.size.height / 2) - (exhaust.size.height / 2) + (50 * scale))
        
        shipRec.position = ship.position
        exhaustRec.position = exhaust.position
        
        self.addChild(exhaust)
        self.addChild(ship)
        self.addChild(shipRec)
        self.addChild(exhaustRec)
        self.addChild(center)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update(dt: TimeInterval) {
        super.update(dt: dt)
    }
}
