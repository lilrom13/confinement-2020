//
//  Entity.swift
//  my game
//
//  Created by Romain Margheriti on 17/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit



// Base class for every others entities
class Entity: SKNode {
    
    init(position: CGPoint) {
        super.init()
        
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(dt: TimeInterval) {
        fatalError("Function update must override by child classes")
    }
}

// Extend the Entity class to add Movement
class MovableEntity: Entity {
    
    internal var vecVelocity: CGVector
    internal var vecVelocityGoal: CGVector
    
    override init(position: CGPoint) {
        self.vecVelocity = CGVector(dx: 0, dy: 0)
        self.vecVelocityGoal = CGVector(dx: 0, dy: 0)
        
        super.init(position: position)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update(dt: TimeInterval) {
        self.vecVelocity.dx = Approach(flGoal: self.vecVelocityGoal.dx, flCurrent: self.vecVelocity.dx, dt: CGFloat(dt * 35));
        self.vecVelocity.dy = Approach(flGoal: self.vecVelocityGoal.dy, flCurrent: self.vecVelocity.dy, dt: CGFloat(dt * 35));

        // Update position and vecVelocity.
        self.position = self.position + self.vecVelocity * CGFloat(dt);
    }
    
    internal func Approach(flGoal: CGFloat, flCurrent: CGFloat, dt: CGFloat) ->CGFloat {
        let flDifference: CGFloat = flGoal - flCurrent

        if (flDifference > dt) {
            return flCurrent + dt
        }
        if (flDifference < -dt) {
            return flCurrent - dt
        }

        return flGoal
    }
}

// Extend IAMovableEntity to create IA
// this class automatically follow a defined path
class IAMovableEntity: MovableEntity {
    
    internal var target: CGPoint
    
    init(position: CGPoint, target: CGPoint) {
        self.target = target
        
        super.init(position: position)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update(dt: TimeInterval) {
        
    }
    
}
