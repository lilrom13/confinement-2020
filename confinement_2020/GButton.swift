//
//  Button.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit

class GButton: SKNode {
    
    var button: SKSpriteNode
    private var mask: SKSpriteNode
    private var cropNode: SKCropNode
    private var action: () -> Void
    private var isEnable = true
    
    init(buttonImage: String, buttonAction: @escaping () -> Void ) {
        button = SKSpriteNode(imageNamed: buttonImage)
        
        self.mask = SKSpriteNode(color: SKColor.black, size: self.button.size)
        self.mask.alpha = 0
        
        self.cropNode = SKCropNode()
        self.cropNode.maskNode = button
        self.cropNode.zPosition = 3
        self.cropNode.addChild(self.mask)
        
        self.action = buttonAction
        
        super.init()
        
        isUserInteractionEnabled = true
        
        self.setUpNodes()
        self.addNodes()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpNodes() {
        self.button.zPosition = 0
    }
    
    private func addNodes() {
        addChild(self.button)
        addChild(self.cropNode)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (isEnable) {
            mask.alpha = 0.5
            
            run(SKAction.scale(by: 1.05, duration: 0.05))
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (isEnable) {
            for touch in touches {
                let location: CGPoint = touch.location(in: self)
                
                if (button.contains(location)) {
                    mask.alpha = 0.5
                }
                else {
                    mask.alpha = 0.0
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (isEnable) {
            for touch in touches {
                let location: CGPoint = touch.location(in: self)
                
                if (button.contains(location)) {
                    self.action()
                    run(SKAction.scale(by: 0.95, duration: 0.05))
                    self.enable()
                }
                else {
                    mask.alpha = 0.0
                }
            }
        }
    }
    
    func desable() {
        self.isEnable = false
        mask.alpha = 0.0
        button.alpha = 0.0
    }
    
    func enable() {
        self.isEnable = true
        mask.alpha = 0.0
        button.alpha = 1.0
    }
}
