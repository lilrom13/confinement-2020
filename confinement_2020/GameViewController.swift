//
//  GameViewController.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    
    var sceneManager : SBSceneManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        

        /// Create an instance of the scene manager
        self.sceneManager = SBSceneManager(view: self.view as! SKView)

        /// Register a scenes
        self.sceneManager?.registerScene(key: "MainMenuScene",
            scene: RMSceneContainer(
                classType: MainMenuScene.self,
                name: "MainMenuScene",
                transition: nil,
                preloadable: true,
                category: RMSceneContainer.SceneGroup.World,
                atlases: ["atlas1", "atlas2"]))

        self.sceneManager?.registerScene(key: "GameScene",
            scene: RMSceneContainer(
                classType: GameScene.self,
                name: "GameScene",
                transition: nil,
                preloadable: true,
                category: RMSceneContainer.SceneGroup.World,
                atlases: ["atlas1", "atlas2"]))
        
        
        /// Display the scene
        if let initialScene = self.sceneManager?.scenes["MainMenuScene"] {
            /// Perform more logic like loading a save file here
            self.sceneManager?.sceneDidFinish(nextScene: initialScene)
        }
    }
}
