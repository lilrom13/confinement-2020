//
//  Utils.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import UIKit

enum UIUserInterfaceIdiom: Int {
    case undefined
    case iphone
    case ipad
}

struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let heigth = UIScreen.main.bounds.size.height
    static let maxHeight = max(ScreenSize.width, ScreenSize.heigth)
    static let minHeight = min(ScreenSize.width, ScreenSize.heigth)
}
