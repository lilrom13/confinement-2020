//
//  File.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import Foundation
import SpriteKit

class GameManager {

    private init() {}
    
    static let shared = GameManager()
    
    public func launch() {
        firstLaunch()
    }
    
    private func firstLaunch() {
        if (!UserDefaults.standard.bool(forKey: "isFirstLaunch")) {
            print("This is the first launch")
            
            UserDefaults.standard.set(true, forKey: "isFirstLaunch")
            UserDefaults.standard.synchronize()
        }
    }
}
