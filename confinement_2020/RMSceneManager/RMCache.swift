//
//  RMCache.swift
//  RMSceneManager
//
//  Created by Romain Margheriti on 22/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit

/**
 Preloading texture atlases is often done on multiple threads, so it is tricky to pass around instances of variables. Global variables are messy and inconsistent. So, this singleton can serve as the consisten location of cache objects for this project, and will mainly be used to store a hard refernece to texture atlases.
*/
public class RMCache : NSCache<AnyObject, AnyObject> {
    
    public static let sharedInstance = RMCache()
    
    private var observer: NSObjectProtocol!
    
    override init() {
        super.init()
        
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil) { [unowned self] notification in
            self.removeAllObjects()
        }
    }
    
    override public func setObject(_ obj: AnyObject, forKey key: AnyObject) {
        super.setObject(obj, forKey: key)
        if let printable = key as? String {
            print("Adding \(printable) to SBCache")
        }
    }
    
    override public func removeAllObjects() {
        super.removeAllObjects()
        print("SBCache emptied")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(observer)
    }
    
}
