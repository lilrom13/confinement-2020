//
//  GameScene.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: RMScene {
    
    private let player = Player(position: CGPoint(x: 50, y: 50))
   
    private var previousTime = TimeInterval()
    private var first = true

    override func didMove(to view: SKView) {
        self.addChild(player)
        self.previousTime = 0
    }
    
    override func update(_ currentTime: TimeInterval) {
        if (!self.first) {
            player.update(dt: currentTime - self.previousTime)
        }
        
        self.previousTime = currentTime
        
        if (self.first) {
            self.first = false
        }
    }
}
