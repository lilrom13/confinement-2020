//
//  MenuScene.swift
//  confinement_2020
//
//  Created by Romain Margheriti on 21/03/2020.
//  Copyright © 2020 Romain Margheriti. All rights reserved.
//

import SpriteKit
import GameplayKit

class MainMenuScene: RMScene {
    
    lazy private var startButton = GButton(buttonImage: "Start_BTN") {
        if let nextScene = self.sbViewDelegate?.scenes["GameScene"] {
            self.sbViewDelegate?.sceneDidFinish(nextScene: nextScene)
        }
    }
    
    override func didMove(to view: SKView) {
        self.addChild(self.startButton)
    }
}
